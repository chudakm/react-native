import React from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, SafeAreaView, FlatList } from "react-native";
import { Contact } from "./contact";
import { useSelector } from "react-redux";
import { selectContacts } from "../../store/contacts";

const renderContactItem = ({ item }) => <Contact {...item} />;

export const ContactsList = () => {
  const contacts = useSelector(selectContacts);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList data={contacts} renderItem={renderContactItem} keyExtractor={item => item.id} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
});
