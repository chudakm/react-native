import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useDispatch } from "react-redux";
import { contactsActions } from "../../store/contacts";

export const Contact = ({ firstName, lastName, phoneNumbers, id }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const onPress = () => {
    dispatch(contactsActions.selectContact({ id }));
    navigation.navigate("Contact");
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.item}>
        <View style={styles.userIcon}>
          <FontAwesome5 style={styles.userIcon} name='user-circle' size={32} color='black' />
        </View>

        <View style={styles.body}>
          <View style={styles.nameArea}>
            <Text style={styles.nameAreaText}>{firstName}</Text>
            <Text style={styles.nameAreaText}>{lastName}</Text>
          </View>

          <View>
            <Text style={styles.nameAreaText}>{phoneNumbers && phoneNumbers[0] && phoneNumbers[0].number}</Text>
          </View>
        </View>

        <View style={styles.info}>
          <AntDesign name='infocirlceo' size={32} color='black' />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    height: 60,
    flexDirection: "row",
    padding: 10,
    backgroundColor: "gray",
    marginVertical: 8,
    marginHorizontal: 16,
  },
  userIcon: {
    alignItems: "center",
    justifyContent: "center",
  },
  body: {
    marginLeft: 10,
    justifyContent: "center",
  },
  nameArea: {
    width: "100%",
    flexDirection: "row",
  },
  nameAreaText: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: "700",
  },
  info: {
    alignItems: "center",
    justifyContent: "center",
    marginLeft: "auto",
  },
});
