import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { useSelector } from "react-redux";
import { selectSelectedContact } from "../../store/contacts";
import { Image } from "react-native";
import { Button } from "react-native";
import { Linking } from "react-native";
import { Entypo } from "@expo/vector-icons";

export const ContactScreen = () => {
  const selectedContact = useSelector(selectSelectedContact);

  const call = () => {
    Linking.openURL(`tel: ${selectedContact.phoneNumbers[0].number}`);
  };

  return (
    <View style={styles.container}>
      <View style={styles.avatarContainer}>
        {selectedContact.image ? (
          <Image
            style={styles.avatar}
            source={{
              uri: selectedContact.image.uri,
            }}
          />
        ) : (
          <Entypo name='user' size={50} color='black' />
        )}
      </View>

      <View style={styles.infoContainer}>
        <View>
          <Text>Name</Text>
          <Text>{selectedContact.name}</Text>
        </View>

        <View style={styles.phoneNumberContainer}>
          <Text>Phone number</Text>
          <Text>{selectedContact.phoneNumbers && selectedContact.phoneNumbers[0] && selectedContact.phoneNumbers[0].number}</Text>
        </View>
      </View>

      <View style={styles.callButtonContainer}>
        <Button style={styles.callButton} onPress={call} title='Call' color='gray' />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
  },
  avatarContainer: { alignItems: "center", justifyContent: "center" },
  avatar: {
    width: 120,
    height: 120,
  },
  infoContainer: {
    marginTop: 50,
    paddingLeft: 20,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  phoneNumberContainer: {
    marginTop: 20,
  },
  callButtonContainer: {
    marginTop: 50,
    width: 100,
    marginLeft: 20,
  },
  callButton: {
    width: 100,
  },
});
