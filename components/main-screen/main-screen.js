import React from "react";
import { StyleSheet, View } from "react-native";
import SearchBar from "react-native-elements/dist/searchbar/SearchBar-ios";
import { useDispatch, useSelector } from "react-redux";
import { contactsActions, selectSearch } from "../../store/contacts";
import { ContactsList } from "../contacts-list/contacts-list";

export const MainScreen = () => {
  const dispatch = useDispatch();
  const search = useSelector(selectSearch);

  const onSearch = search => dispatch(contactsActions.updateSearch(search));

  return (
    <View style={styles.container}>
      <SearchBar placeholder='Type Here...' onChangeText={onSearch} value={search} />
      <ContactsList />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
  },
});
