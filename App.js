import "react-native-gesture-handler";
import React, { useEffect } from "react";
import * as Contacts from "expo-contacts";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Provider, useDispatch } from "react-redux";
import { store } from "./store/store";
import { MainScreen } from "./components/main-screen/main-screen";
import { contactsActions } from "./store/contacts";
import { ContactScreen } from "./components/contact-screen/contact-screen";

const Stack = createStackNavigator();

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === "granted") {
        const { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.Emails, Contacts.Fields.PhoneNumbers, Contacts.Fields.Image],
        });

        if (data.length) dispatch(contactsActions.contactsReceived({ contacts: data }));
      }
    })();
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name='Main' component={MainScreen} />
          <Stack.Screen name='Contact' component={ContactScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default function AppWrapper() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}
