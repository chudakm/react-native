import { createEntityAdapter, createSelector, createSlice } from "@reduxjs/toolkit";

const contactsAdapter = createEntityAdapter({});

const contactsSlice = createSlice({
  name: "contacts",
  initialState: contactsAdapter.getInitialState({
    search: "",
    selectedContact: null,
  }),
  reducers: {
    updateSearch(state, action) {
      state.search = action.payload;
    },
    contactsReceived(state, action) {
      contactsAdapter.setAll(state, action.payload.contacts);
    },
    selectContact(state, action) {
      state.selectedContact = state.entities[action.payload.id];
    },
  },
});

export const contactsReducer = contactsSlice.reducer;

export const contactsActions = contactsSlice.actions;

const contactsSelector = state => state.contacts;
const adapterSelectors = contactsAdapter.getSelectors(state => state.contacts);

export const selectContacts = state => {
  const search = state.contacts.search;
  const contacts = adapterSelectors.selectAll(state);
  return contacts.filter(contact => contact.name.includes(search));
};
export const selectSearch = createSelector(contactsSelector, state => state.search);
export const selectSelectedContact = createSelector(contactsSelector, state => state.selectedContact);
